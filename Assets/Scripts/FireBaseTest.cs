﻿using Firebase;
using Firebase.Analytics;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FireBaseTest : MonoBehaviour
{

    public void SaveDataFirebase(string id, string name, string station)
    {
        var mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        mDatabaseRef.Child(id).Child("Station").SetValueAsync(name);
        mDatabaseRef.Child(id).Child("Weather").SetValueAsync(station);
    }

    public void GetDataFirebase()
    {
        FirebaseDatabase.DefaultInstance.GetReference("0").GetValueAsync()
            .ContinueWith(task =>
            {
                if (task.IsFaulted) Debug.Log("ERROR... Firebase");
                else if (task.IsCompleted)
                    foreach (var rules in task.Result.Children)
                        Debug.Log(rules.Key + " : " + rules.Value);

            });
    }
    public void UpdateFirebase(string id, string name, string station)
    {
        var mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        mDatabaseRef.Child(id).Child("Station").SetValueAsync(name);
        mDatabaseRef.Child(id).Child("Weather").SetValueAsync(station);
    }
    public void DeleteFirebase()
    {
        var mDatabaseRef =
      FirebaseDatabase.DefaultInstance.RootReference;
        mDatabaseRef.RemoveValueAsync();
    }

    

}

